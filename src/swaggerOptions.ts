/**
 * @ignore
 */
export const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "encuesta.bo Software Rest API",
            version: "1.0.0",
            description: "Rest API for 'encuesta.bo' software"
        },
        servers: [
            {
                url: "http://localhost:3000"
            }
        ]
    },
    apis: [
        "./src/api/**/routes/*.ts",
        "./src/*.ts"
    ]
}