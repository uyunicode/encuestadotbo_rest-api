export class TypeUser {
    public static USER_POLLSTER: string = "user_pollster";
    public static USER_OPERATOR: string = "user_operator";
    public static USER_CLIENT: string = "user_client";
}