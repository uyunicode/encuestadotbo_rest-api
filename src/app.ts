/**
 * Import dependencies
 */
import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import swaggerUI from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';
import './config/database/connection.mongo';

/**
 * Import settings files
 */
import { options } from './swaggerOptions';
// import { AuthJsonWebToken } from './middlewares/auth';

/**
 * Import Routers
 */
import pollsterUserRouter from './api/user_management/pollster_manage/routes/pollster.routes';


/**
 * @class App for the initialization of the web server
 * and its settings and middlewares
 */
export class App {

    /**
     * Attributes
     */
    private app: Application;

    /**
     * Constructor that initializes express, middlewares and routes
     * @constructor 
     * @param port port for initialize server
     */
    constructor(port?: number | string) {
        this.app = express();
        this.app.set('PORT', process.env.PORT || port || 3000);
        this.middlewares();
        this.routes();
    }

    /**
     * @private
     * Run these processes before every Request
     * @returns nothing, is a void
     */
    private middlewares(): void {
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
    }

    /**
     * @private
     * Method for starting routes for HTTP services
     * @returns nothing, is a void
     */
    private routes(): void {
        // swagger ui server
        let specs = swaggerJsDoc(options);
        /**
         * @swagger
         * /docs :
         *  get:
         *      summary: Returns the web server documentation
         *      responses:
         *          200:
         *              description: Obtain the documentation of the enabled routes
         *              content:
         *                  html:
         *                      schema:
         *                          type: html
         *                          description: doc swagger
         */
        this.app.use('/docs', swaggerUI.serve, swaggerUI.setup(specs));
        // listen routers
        this.app.use('/user_management/pollster_manage', pollsterUserRouter);
    }

    /**
     * @public
     * Asynchronous process for port initialization with the web server
     * @returns nothing, is a void
     */
    public async listen(): Promise<void> {
        await this.app.listen(this.app.get('PORT'));
        console.log("\x1b[46m", "\x1b[30m", `Server on port ${this.app.get('PORT')}`, "\x1b[0m");
    }
}