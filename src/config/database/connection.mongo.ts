import mongoose from 'mongoose';

mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/encuesta_bo_rest_api', {
    // useNewUrlParser: true,
    // useUnifiedTopology: true,
    // useCreateIndex: true
})
    .then(db =>
        console.log("DATABASE " + process.env.MONGODB_URI + " connected successfully"))
    .catch(err =>
        console.log("Error connection mongoDB: " + process.env.MONGODB_URI, err));