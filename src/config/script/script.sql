-- class: topics advanced programming
-- UAGRM - FICCT
-- @author: cruz rodriguez zuleny, ruddy bryan quispe mamani 
-- @version: 0.0.1
-- @since: 21-10-2021

-- ******** CREACION DEL USUARIO O ROL ******** 

CREATE ROLE encuesta_bo
   WITH
   LOGIN
   INHERIT
   REPLICATION
   CONNECTION LIMIT -1
   PASSWORD 'b62Zx8z6-feq7ñ-4a86-12aeq-107d0865593F';

-- ********* CREACION DE LA BD *************
CREATE DATABASE encuesta_bo_rest_api
   with owner=encuesta_bo
   encoding='UTF8'
   tablespace=pg_default
   CONNECTION LIMIT=-1;

create table pollster_user (
	id serial primary key,
	name text not null,
   email text not null,
	cellphone varchar(15) not null,
	password text not null,
	state boolean not null
);

create table classification (
	id serial primary key,
	description text not null
);

create table operator_user(
	id serial primary key,
	name text not null,
	email text not null,
	password text not null
);

create table client_user (
	id serial primary key,
	name text not null,
	email text not null,
	password text not null,
	emapresa text not null
);

create table poll (
	code serial primary key,
	title text not null,
	description text not null,
	status boolean not null,
	id_classification int not null,
	id_client_user int not null,
	id_operator_user int not null,
	foreign key (id_classification) references classification(id)
	on update cascade
	on delete cascade,
	foreign key (id_client_user) references client_user(id)
	on update cascade
	on delete cascade,
	foreign key (id_operator_user) references operator_user(id)
	on update cascade
	on delete cascade
);

create table section(
	id serial primary key,
	title text not null,
	code_poll int not null,
	foreign key (code_poll) references poll(code)
	on update cascade
	on delete cascade
);

create table question (
	code serial not null,
	id_section int not null,
	title text not null,
	type_option int not null,
	required boolean not null,
	primary key (code, id_section),
	foreign key (id_section) references section(id)
	on update cascade
	on delete cascade
);

create table answer_option (
	id serial primary key,
	opcion text not null,
	code_question int not null,
	id_section_question int not null,
	foreign key (code_question,id_section_question) references question(code, id_section)
	on update cascade
	on delete cascade
);

create table survey_tab (
	no_tab serial primary key,
	date_time timestamp not null,
	id_pollster_user int not null,
	code_poll int not null,
	foreign key (id_pollster_user) references pollster_user(id)
	on update cascade
	on delete cascade,
	foreign key (code_poll) references poll(code)
	on update cascade
	on delete cascade
);

create table answer (
	id serial not null,
	no_tab_survey int not null,
	detail text not null,
	code_question int not null,
	id_section_question int not null,
	primary key (id, no_tab_survey),
	foreign key (no_tab_survey) references survey_tab(no_tab)
	on update cascade
	on delete cascade,
	foreign key (code_question,id_section_question) references question(code,id_section)
	on update cascade
	on delete cascade
);