/**
 * data type pollster (Movil App User)
 */
export interface Pollster {
    id: number,
    name: string,
    email: string,
    cellphone: string,
    password: string,
    state?: boolean
}