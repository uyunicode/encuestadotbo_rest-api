import { Schema, model, Document } from 'mongoose'

export interface IPollsterUser extends Document {
    name: string;
    email: string;
    cellphone: string;
    password: string;
    state: boolean;
};

const userSchema = new Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        lowercase: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    cellphone: {
        type: String,
        max: 8,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    state: {
        type: Boolean
    }
}, {
    timestamps: true
});

export default model<IPollsterUser>('PollsterUser', userSchema);