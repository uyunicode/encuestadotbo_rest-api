/**
 * Import Dependencies
 */
import { QueryResult } from "pg";


/**
 * Import Components
 */
import { Connection } from "../../../../config/database/connection.pgsql";
import { Pollster } from "./pollster";

/**
 * @class Model for the administration of pollster user data.
 */
export class PollsterModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Method that returns information from a pollster user with the identification email.
     * @param email : email to get data info
     * @returns pollster data or if not find data return undefined
     * @catch undefined if has error in SQLquery
     */
    public findByEmail = async (email: string): Promise<Pollster | undefined> => {
        try {
            // execute sqlQuery
            let userPollsterAccount: QueryResult = await this.connection.executeQuerySQL(
                `select id,"name",email,cellphone,"password",state 
                from pollster_user
                where email='${email}'`);
            return userPollsterAccount.rows[0];
        } catch (error) {
            console.log("Error in Pollster Model -> findByEmail", error);
            return undefined;
        }
    }

    /** Method that returns information from a pollster user with the identification Id.
     * @param id  : id to get data info
     * @returns pollster data or if not find data return undefined
     * @catch undefined if has error in SQLquery
     */
    public findById = async (id: number): Promise<Pollster | undefined> => {
        try {
            // execute sqlQuery
            let userPollsterAccount: QueryResult = await this.connection.executeQuerySQL(
                `select id,"name",email,cellphone,"password",state 
                from pollster_user
                where id=${id}`);
            return userPollsterAccount.rows[0];
        } catch (error) {
            console.log("Error in Pollster Model ->  findById", error);
            return undefined;
        }
    }

    /**
     * Register new user possible victim
     * @param accountPollsterUser data new user pollster 
     * @returns true if saved success else false if has a problem to saved or query invalidated
     */
    public create = async (accountPollsterUser: Pollster): Promise<boolean> => {
        try {
            let idNewAccount = await this.connection.executeQuerySQL(
                `insert into pollster_user("name",email,cellphone,"password",state) 
                values ('${accountPollsterUser.name}', 
                '${accountPollsterUser.email}', ${accountPollsterUser.cellphone}, '${accountPollsterUser.password}', true) returning id;`
            );
            return idNewAccount.rows[0].id > 0 ? true : false;
        } catch (error) {
            console.log("Error in Pollster Model -> create(): ", error);
            return false;
        }
    }

    /**
     * Get List pollster accounts saved into database
     * @returns list POllster accounts saved
     */
    public getAll = async (): Promise<Array<Pollster>> => {
        let result = await this.connection.executeQuerySQL(
            `select id,"name",email,cellphone,"password",state from pollster_user;`
        );
        return result.rows;
    }
}