/**
 * Import Components 
 */
import { TypeUser } from "../../../../middleware/type_account";
import { AuthJsonWebToken } from "../../../../middleware/auth";
import { Encrypt } from "../../../../services/encrypt";
import { Pollster } from "../model/pollster";

/**
 * Import Models
 */
import { PollsterModel } from "../model/pollster.model";
import PollsterUserNoSQL, { IPollsterUser } from '../model/n_pollster.model';

/**
 * @class of business logic implementation for pollster manage services users.
 */
export class PollsterService {

    /**
     * Attributes
     */
    private pollsterModel: PollsterModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.pollsterModel = new PollsterModel();
    }

    /**
     * This method does the verification of the police login.
     * @param email : email account to verify
     * @param password : password account to verify
     * @returns result if accept session and return JWT but if denied sesion return message error
     * @catch return an error 500 http to have an error to verify
     */
    public authSessionPollsterUser = async (email: string, password: string): Promise<any> => {
        try {
            let pollsterAccount: Pollster | undefined = await this.pollsterModel.findByEmail(email);
            if (typeof pollsterAccount !== 'undefined') {
                if (!pollsterAccount.state)
                    return { status: 203, msg: "account not enabled" }
                if (await Encrypt.comparePassword(password, pollsterAccount.password)) {
                    let jwtSession: string = AuthJsonWebToken.generateToken(pollsterAccount.id, TypeUser.USER_POLLSTER);
                    return {
                        status: 200,
                        msg: "session success",
                        jwt: jwtSession
                    };
                } else {
                    return {
                        status: 401,
                        msg: "password incorrect"
                    };
                }
            } else {
                return {
                    status: 401,
                    msg: "account not found"
                };
            }
        } catch (error) {
            console.log("Error in possible victim service authSessionPollsterUser()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * This method does the verification of the police login.
     * @param email : email account to verify
     * @param password : password account to verify
     * @returns result if accept session and return JWT but if denied sesion return message error
     * @catch return an error 500 http to have an error to verify
     */
    public authSessionPollsterUserNoSQL = async (email: string, password: string): Promise<any> => {
        try {
            let pollsterAccount: IPollsterUser | undefined = await PollsterUserNoSQL.findOne({ email });
            if (typeof pollsterAccount !== 'undefined') {
                if (!pollsterAccount.state)
                    return { status: 203, msg: "account not enabled" }
                if (await Encrypt.comparePassword(password, pollsterAccount.password)) {
                    let jwtSession: string = AuthJsonWebToken.generateToken(pollsterAccount.id, TypeUser.USER_POLLSTER);
                    return {
                        status: 200,
                        msg: "session success",
                        jwt: jwtSession
                    };
                } else {
                    return {
                        status: 401,
                        msg: "password incorrect"
                    };
                }
            } else {
                return {
                    status: 401,
                    msg: "account not found"
                };
            }
        } catch (error) {
            console.log("Error in possible victim service authSessionPollsterUser()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * Register new account for pollster user
     * @param Pollster : user pollster data to save
     * @returns json response http with jwt if success save
     */
    public registerNewPollsterUserAccount = async (pollster: Pollster): Promise<any> => {
        try {
            pollster.password = await Encrypt.encryptPassword(pollster.password);
            // falta verificar datos del usuario
            if (this.pollsterModel.findById(pollster.id) === undefined) {
                return {
                    status: 401,
                    msg: "account exists"
                }
            }
            if (await this.pollsterModel.create(pollster)) {
                let jwtSession: string = AuthJsonWebToken.generateToken(pollster.id, "pollster_account");
                return {
                    status: 201,
                    msg: "new account saved successfully",
                    jwt: jwtSession
                }
            } else {
                return {
                    status: 500,
                    msg: "i have problems to register new account"
                };
            }
        } catch (error) {
            console.log("Error in possible Pollster Service -> create()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * get list pollster
     * @returns list pollster info
     */
    public getListPollsterAccount = async () => {
        let listUsers = await this.pollsterModel.getAll();
        return {
            status: 200,
            msg: "get list success",
            data: listUsers
        }
    }
}