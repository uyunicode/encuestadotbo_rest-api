/**
 * Import Dependencies
 */
import { Request, Response } from "express";

/**
 * Import Services
 */
import { PollsterService } from '../service/pollster.service';

/**
 * @class controller pollster user, for the implementation of HTTP requests
 */
export class PollsterController {

    /**
     * Attributes
     */
    private pollsterService: PollsterService;

    /**
     * @constructor
     * Initialize pollster user service for implement business logic
     */
    public constructor() {
        this.pollsterService = new PollsterService();
    }

    /**
     * This method returns the response of whether the login is accepted or not.
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public signIn = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { email, password } = request.body;
            let resultSession = await this.pollsterService.authSessionPollsterUser(email, password);
            return response.status(resultSession.status).json(resultSession);
        } catch (error) {
            console.log("Error in signIn pollster controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * register new account to pollster
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json response to client
     */
    public signUp = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { name, email, cellphone, password } = request.body;
            let responseRegister = await this.pollsterService.registerNewPollsterUserAccount({ id: -1, name, email, cellphone, password });
            return response.status(responseRegister.status).json(responseRegister);
        } catch (error) {
            console.log("Error in pollster controller register", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * 
     * @param request : HTTP request
     * @param response : HTTP response
     * @returns list pollster
     */
    public getListPossibleUsers = async (request: Request, response: Response): Promise<Response> => {
        let responseListUsers = await this.pollsterService.getListPollsterAccount();
        return response.status(responseListUsers.status).json(responseListUsers);
    }
}