/**
 * Import Libraries
 */
import { Router } from 'express';
import { AuthJsonWebToken } from '../../../../middleware/auth';

/**
 * Import Controllers
 */
import { PollsterController } from '../controller/pollster.controller';

// initialize Router
const routes = Router();
// initialize controllers
const pollsterController: PollsterController = new PollsterController();

routes.get('/some_route', (req, res) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log(req);
    console.log(ip); // ip address of the user
    res.send(ip);
});

/**
 * @swagger
 * /user_management/pollster_manage/sign_in :
 *  post:
 *      summary: This performs the process of beginning session (login) for the users pollster
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.post('/sign_in', pollsterController.signIn);

/**
 * @swagger
 * /user_management/possible_victim_manage/sign_up :
 *  post:
 *      summary: This performs the process register new account pollster user (mobile user)
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *      responses:
 *          200:
 *              description: returns jwt if saved successfully but if not saved send http status 500 or 400
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: object
 *                                  description: return jwt if is login success; but another, no returns jwt
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.post('/sign_up', pollsterController.signUp);

/**
 * @swagger
 * /user_management/possible_victim_manage/get_lists/:ci_victim :
 *  get:
 *      summary: get list friends trusted person
 *      responses:
 *          200:
 *              description: returns possible victim data
 *              content:
 *                  application/json:
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 * 
 */
routes.get('/get_all', pollsterController.getListPossibleUsers);

export default routes;