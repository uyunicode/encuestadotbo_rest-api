# SOFTWARE DE GESTIÓN ENCUESTAS encuesta.bo

_Project for the subject of topics advance programming at the UAGRM - FICCT._
_SOFTWARE DE GESTION DE ENCUESTAS "encuesta.bo"_

## Comenzando 🚀

_A continuación, estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.

### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node JS version LTS
PostgreSQL
Navegador Web
Dispositivo Movil Android o iOS
```

### Instalación 🔧

_Cargar la BD en un motor SQL PostgreSQL_

_\* el codigo SQL se encuentra en src/database/\*.sql_

_Paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

```
cd backend
yarn install
yarn run dev
```

_Y detener la compilación_

```
Ctrl+c
```

_Si no funciona._

```
Ctrl+Alt+Supr
```

## Ejecutando las pruebas ⚙️

_Ya esta explicado alli arriba_

### Analice las pruebas end-to-end 🔩

_Buscar en google (http://aun_no_hay_link.lan)_

### Y las pruebas de estilo de codificación ⌨️

_SOFTWARE DE GESTIÓN ENCUESTAS "encuesta.bo"_
_Este software web esta dividido en 3 partes, Rest-API backend, front-end web y front-end mobile_

## Despliegue 📦

_pm2_
_Nginx_

## Construido con 🛠️

_herramientas que utiliza para crear tu proyecto_

- [NodeJS](https://nodejs.org/en/) - Compilador de JSen Desktop
- [Express](http://expressjs.com/) - Framework Backend
- [PostgreSQL](https://www.postgresql.org/) - Motor de Base de Datos

## Contribuyendo 🖇️

Por favor lee el [Documentación](https://google.com) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Versionado 📌

Usamos [Git](http://git.org/) para el versionado.

## Autores ✒️

_Agradecido con el de Arriba_

- **Cruz Rodriguez Zuleny** - _Scrum Master_ - [Github](https://github.com/Zuleny) [Bitbucket](https://bitbucket.org/zuleny/)
- **Quispe Mamani Ruddy Bryan** - _Development Jr_ - [Github](https://github.com/RuddyQuispe) [Bitbucket](https://bitbucket.org/ruddyq/)

## Expresiones de Gratitud 🎁

- Agradecido con el de Arriba 📢

---

⌨️ con ❤️ por el Team Dev 😊
